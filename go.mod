module googleradius

require (
	github.com/bronze1man/radius v0.0.0-20180530005926-721cfbe34854 // indirect
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/gorilla/sessions v1.1.3
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/satori/go.uuid v1.2.0
	golang.org/x/oauth2 v0.0.0-20190130055435-99b60b757ec1
	gopkg.in/go-playground/validator.v9 v9.26.0
	layeh.com/radius v0.0.0-20190118135028-0f678f039617
)
