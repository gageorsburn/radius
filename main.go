package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"layeh.com/radius"
	"layeh.com/radius/rfc2865"
	"log"
	"net/http"
	"strconv"
)

func handle(w radius.ResponseWriter, r *radius.Request) {
	username := rfc2865.UserName_GetString(r.Packet)
	password := rfc2865.UserPassword_GetString(r.Packet)

	var code radius.Code

	credentials := struct {
		Email string `json:"email"`
		Token string `json:"token"`
	}{
		Email: username,
		Token: password,
	}

	b, _ := json.Marshal(credentials)

	resp, err := http.Post("http://token/api/token/verify", "application/json", bytes.NewBuffer(b))

	if err != nil {
		log.Fatalln(err)
	}

	log.Printf("%+v", resp)

	b, _ = ioutil.ReadAll(resp.Body)
	body := string(b)

	log.Println(body)

	valid, _ := strconv.ParseBool(body)

	if valid {
		code = radius.CodeAccessAccept
	} else {
		code = radius.CodeAccessReject
	}

	w.Write(r.Response(code))
}

func main() {

	server := radius.PacketServer{
		Handler:      radius.HandlerFunc(handle),
		SecretSource: radius.StaticSecretSource([]byte(`secret`)),
	}

	log.Printf("Starting server on :1812")
	if err := server.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}
