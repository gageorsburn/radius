build:
	docker build -t onesourceintegrations/radius:latest .

push:
	docker push onesourceintegrations/radius:latest

deploy:
	kubectl apply -f deploy.yml -n vpn