FROM golang:1.11.5 as builder
ENV GO111MODULE=on
ADD . /app
WORKDIR /app
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o radius *.go

FROM golang:alpine as production
RUN apk add --no-cache ca-certificates openssl
COPY --from=builder /app/radius /
CMD ["/radius"]